# udev-serial

## Usage

```js
const udevSerial = require("udev-serial");

var serial = udevSerial({
  DEVNAME: true,
  ID_MODEL_FROM_DATABASE: /UART Bridge/,
  SUBSYSTEM: "tty"
}, {
  delay: 0,
  baudRate: 115200
});

serial.on("open", function() {
  console.log("port opened");
});

serial.on("data", function(data) {
  console.log(data.toString());
});
```

### `udevSerial(descriptor, [ opts ])`

`descriptor` is an object used to match against udev events. It's keys
may be a `String`, `Number`, `RegExp` or `true`/`false`. *Note:* `Number`
is coerced using `==` as udev event values will always be strings.

`opts.delay` is the time in milliseconds that should be delayed from the
udev event to opening the serial port.

`opts.baudRate` baud rate passed to the serial port constructor.

`opts.wait` if set to `true` udev-serial will not check current devices.

`opts.noConnect` if set to `true` udev-serial will not automatically connect.

`opts.noReconnect` is set to `true` udev-serial will stop the `udev.monitor`

`opts.SerialPort` Override SerialPort constructor. Used for testing.

`opt.udev` Override udev module. Used for testing.


## CLI
```
Usage: udev-serial -v key=value,... {OPTIONS}

    --baud,  -b Baud rate for node-serialport

    --value, -v Set value to match udev events against
                example: --value DEVNAME=true

    --list,  -l List devices
                Can filter with -v

    --return,
             -r Key to return on match

    --json,  -j Return JSON (ignores -r)
```

### CLI usage example
```
$ udevserial -r DEVNAME -v DEVNAME=true -v ID_MODEL=/duino/ -v SUBSYSTEM="tty" -l
/dev/ttyACM0
$
```
