const debug = require("debug")("udev-serial");
const EventEmitter = require("events").EventEmitter;
const udev = require("udev");
const SerialPort = require("serialport");

function E_CONN_ESTB() {
  this.message = "Connection Already Established";
}

E_CONN_ESTB.prototype = new Error();

module.exports = function(devSpec, opts) {
  opts = (opts || {});

  var sp;
  var connected = false;
  var ee = new EventEmitter();
  var specKeys = Object.keys(devSpec);
  debug(specKeys, "specKeys");

  var baudRate = opts.baudRate;
  var delay = opts.delay || 300;

  if(opts.monitor !== false) {
    var monitor = udev.monitor();

    // Opens a monitor that closes when it receives an add -event.
    monitor.on("remove", function(device) {
      if(compareSpec(device)) {
        sp.close(function() {
        });
      }
    });

    monitor.on("add", function (device) {
      if(compareSpec(device)) {
        ee.emit("found", device);

        connectPort(device);
      }
    });
  }

  if(opts.wait !== true) {
    setImmediate(function() {
      var list = udev.list();
      list.forEach(function(device) {
        if(compareSpec(device)) {
          ee.emit("found", device);

          if(opts.autoConnect !== false) {
            connectPort(device);
          }
        }
      });
    });
  }

  return ee;

  function connectPort(device) {
    if(opts.noConnect) return;

    if(connected) {
      return ee.emit("error", new E_CONN_ESTB());
    }

    sp = new SerialPort(device.DEVNAME, {
      autoOpen: false,
      baudRate: baudRate
    });

    debug({
      port: device.DEVNAME,
      baudRate: baudRate
    }, "attempting serial connection");

    if(opts.noReconnect) {
      sp.on("close", function() {
        if(opts.monitor !== false) {
          monitor.close();
        }
      });
    }

    sp.on("open", function() {
      debug({
        port: device.DEVNAME,
        baudRate: baudRate
      }, "serial port connected");

      connected = true;
      ee.emit("open", sp);
    });

    sp.on("data", ee.emit.bind(ee, "data"));
    sp.on("error", ee.emit.bind(ee, "error"));
    sp.on("close", ee.emit.bind(ee, "close"));
    sp.on("disconnect", function() {
      connected = false;

      debug({
        port: device.DEVNAME,
        baudRate: baudRate
      }, "serial port disconnected");
    });

    setTimeout(sp.open.bind(sp), delay);
  }

  function compareSpec(dev) {
    for(var i = 0; i < specKeys.length; i++) {
      var k = specKeys[i];
      var v = devSpec[k];
      var t = typeof v;

      if(t === "boolean") {
        if(!!dev[k] !== v) return false;
      } else if(t === "number") {
        if(dev[k] != v) return false;
      } else if(t === "string") {
        if(dev[k] !== v) return false;
      } else if(v instanceof RegExp) {
        if(!v.test(dev[k])) return false;
      }
    }

    return true;
  }
};
