const debug = require("debug")("udev-serial-demo");
const udevSerial = require("./");

var serial = udevSerial({
  DEVNAME: true,
  ID_MODEL_FROM_DATABASE: /Teensyduino/,
  SUBSYSTEM: "tty"
}, {
  delay: 0,
  baudRate: 115200
});

serial.on("data", function(data) {
  debug({
    data: data.toString(),
  }, "data event");
});

serial.on("open", function() {
  debug("port opened");
});

serial.on("disconnect", function() {
  console.log("port closed");
});
