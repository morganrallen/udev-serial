#!/usr/bin/env node
/* eslint no-console: 0 */

var autoParse = require("auto-parse");
var debug = require("debug")("udev-serial-cli");
var isRegExp = require("regex-regex");
var fs = require("fs");
var udevSerial = require("../");

var args = require("minimist")(process.argv, {
  alias: {
    b: "baud",
    l: [ "ls", "list" ],
    r: "return",
    v: "value"
  },
  default: {
    return: "DEVNAME"
  }
});

if(!args.v && !args.l) {
  return usage();
}

var descriptor = {};
var opts = {
  noConnect: true,
  baudRate: args.b
};

if(args.v && !Array.isArray(args.v)) args.v = [ args.v ];

if(args.v) {
  args.v.forEach(function(arg) {
    var [k, v] = arg.split("=");

    if(isRegExp.test(v)) {
      descriptor[k] = new RegExp(v.slice(1, -1));
    } else {
      descriptor[k] = autoParse(v);
    }
  });
}

if(args.l) {
  opts.autoConnect = false;
  opts.monitor = false;
}

debug(descriptor, "dev descriptor");
debug(opts, "serial opts");

var serial = udevSerial(descriptor, opts);

serial.on("found", function(device) {
  if(!args.j && (args.l && device[args.r])) {
    return console.log(device[args.r]);
  } else if(args.j) {
    console.log(device);

    //process.exit(0);
  }
});

function usage() {
  fs.createReadStream(__dirname + "/usage.txt").pipe(process.stdout).on("end", function() {
    process.exit(1);
  });
}
